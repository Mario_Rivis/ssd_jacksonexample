#Download and Install

On this page, click the **Downloads** tag and download the repository.

Unzip the repository in a local directory. Then open a command line window and use the `cd` command to got to the folder that was unzipped.

Run `gradlew build` in that folder. It should finally print out `BUILD SUCCESS`.

#Import the project to your IDE

##Eclipse

Follow this link to learn to import Gradle projects in Eclipse:
http://www.vogella.com/tutorials/EclipseGradle/article.html

##IntelliJ IDEA
Follow this link to learn how to import Gradle projects in IntelliJ IDEA:
https://www.jetbrains.com/help/idea/gradle.html#gradle_import_project_start


You are now ready to develop your Jackson Features.
#Task

In the `src/main/java` folder, you have a `upt.ssd.Main` class that contains the main method, where you have a TODO, that specifies the task.

You will find the Student and Course model classes in the `upt.ssd.model` package. If you want to experiment, you can create more classes and test them to see how Jackson Json Mapper behaves.

Follow this link for a tutorial on Jackson and try to implement the TODO:
http://www.baeldung.com/jackson-object-mapper-tutorial