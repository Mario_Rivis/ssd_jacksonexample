package upt.ssd.model;

public class Course {

    private String acronym;
    private String description;

    public Course() {
    }

    public Course(String acronym, String description) {
        this.acronym = acronym;
        this.description = description;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getDescription() {
        return description;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Course{" +
                "acronym='" + acronym + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
